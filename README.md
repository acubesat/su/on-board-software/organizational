## Description

A repository to elevate GitLab features such as [Scoped Labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels) and [Related Issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) to use GitLab as an organizational work for all (most) mission software-related work (and in a transparent manner, whenever possible).

---

<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

---

## Table of Contents

<details>
<summary>Click to expand</summary>

[[_TOC_]]

</details>

## Features

### Repositories

You will find five projects (repositories) in [`Mission Software`](https://gitlab.com/acubesat/su/on-board-software):
- [`Embedded Software`](https://gitlab.com/acubesat/su/on-board-software/su-software): Here you will find all (to be) on-board SW to carry out the scientific mission (to be ran on the SU PCB MCU).
- [`Component Drivers`](https://gitlab.com/acubesat/su/on-board-software/component-drivers): Here is a barebones repository, containing the code for each payload component driver. The purpose of this repository is to be used as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). Therefore, all necessary boilerplate code will be found elsewhere, when that time comes.
- [`Organizational`](https://gitlab.com/acubesat/su/on-board-software/organizational): _seem familiar?_
- [`Compression Software`](https://gitlab.com/acubesat/su/on-board-software/compression-software): Here you will find the compression scheme implementation to be used on-board. This is needed in order to compress the captured images of the fluorescing cells inside the PDMS microfluidic Lab-On-a-Chip (LOC) _before_ downlinking them to Earth. Something something link budgets.
- [`Compression Algorithms Analysis`](https://gitlab.com/acubesat/su/on-board-software/compression): This stays mostly for historical purposes. It contains some preliminary benchmarking for some compression schemes; you can find more in [`DDJF_PL, Appendix Section E, Image Compression, Subsection 11, Benchmarking/Testing`](https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf).

### Issues

You will find all issues relating to the mission software [here](https://gitlab.com/groups/acubesat/su/on-board-software/-/issues).

### Milestones

You can find our milestones [here](https://gitlab.com/groups/acubesat/su/on-board-software/-/milestones):
- [`[SW] Implement Drivers`](https://gitlab.com/groups/acubesat/su/on-board-software/-/milestones/4): This keeps track of implementing all the drivers for the components that only exist on the payload, and nowhere else inside the AcubeSAT nanosatellite. All drivers for components that exist elsewhere (e.g. CAN transceiver) will be copied instead from the [OBC codebase](https://gitlab.com/acubesat/obc/atsam-component-drivers).
- [`[SW] Integrate ECSS Services`](https://gitlab.com/groups/acubesat/su/on-board-software/-/milestones/3): This keeps track of integrating the various [ECSS services](https://gitlab.com/acubesat/obc/ecss-services) needed in the mission software codebase.
- [`[SW] Copy Code from OBC Software`](https://gitlab.com/groups/acubesat/su/on-board-software/-/milestones/2): This keeps track of all the things that need to be copied over from OBC (i.e. all issues with the label ~"Copy"); e.g. copying the MCU configuration files, or the script to send TCs and receive TMs from/to a PC.
- [`[SW] Implement Gatekeeper Tasks for Every Driver`](https://gitlab.com/groups/acubesat/su/on-board-software/-/milestones/1): This keeps track of all the Gatekeeper Tasks (FreeRTOS, see [this](https://www.freertos.org/FreeRTOS_Support_Forum_Archive/August_2013/freertos_Gate_Keeper_task_8629629.html#:~:text=Gatekeeper%20is%20just%20a%20term,do%20it%20through%20the%20gatekeeper.) for more) that need to be implemented, one for each component driver.

### Epics

You can find our epics [here](https://gitlab.com/groups/acubesat/su/-/epics?state=opened&page=1&sort=start_date_desc):
- [`SU Embedded Software Initial Configuration`](https://gitlab.com/groups/acubesat/su/-/epics/8): This keeps track of integrating things that already exist, either as-is or with small modifications.
- [`SU Peripheral Drivers`](https://gitlab.com/groups/acubesat/su/-/epics/6): This keeps track of things that relate to component driver development.
- [`SU Business Logic`](https://gitlab.com/groups/acubesat/su/-/epics/7): This keeps track of things that need to be worked on from scratch.

All three of these epics are children of the [`Payload Campaign Software`](https://gitlab.com/groups/acubesat/su/-/epics/5) epic.

For information on the other boards, see [here](https://gitlab.com/acubesat/su/organizational#boards).

### Boards

#### Status Board

Click [here](https://gitlab.com/groups/acubesat/su/on-board-software/-/boards/4342046) to visit our [issue board](https://docs.gitlab.com/ee/user/project/issue_board.html) (`Status`) that functions as a [`Kanban` board](https://en.wikipedia.org/wiki/Kanban_(development)).
Here, the issues (i.e. tasks) are in the form of movable cards. You will find some columns, each corresponding to a `Status::xyz` label.
You can move a task card between columns, and the status (i.e. the label) will also change automatically.

#### Milestones Board

The [`Milestones`](https://gitlab.com/groups/acubesat/su/on-board-software/-/boards/4341818) board displays issues grouped according to which milestone they belong to.

#### SW Board

The [`SW`](https://gitlab.com/groups/acubesat/su/on-board-software/-/boards/4341811) board displays issues grouped according to the `SW::` scoped labels (e.g. ~"SW::Driver", or ~"SW::FreeRTOS").

### Roadmap

You can find our roadmap [here](https://gitlab.com/groups/acubesat/su/-/roadmap?state=opened&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL). This is WIP and subject to change soon.

### Labels

We use labels for characterizing and categorizing all tasks. You can take a look at all the labels and their descriptions by clicking [here](https://gitlab.com/groups/acubesat/su/on-board-software/-/labels)

#### Mission Software-Specific Labels

- ~"Copy" | `Copy from OBC`
- ~"SW::Compression" | `This task is related to the on-board compression scheme`
- ~"SW::Driver" | `This task is related to the development of a driver`
- ~"SW::ECSS" | `This task is related to the ECSS services`
- ~"SW::FreeRTOS" | `This task is related to FreeRTOS`
- ~"SW::MCU" | `This task is related to the microcontroller`
- ~"SW::Misc" | `This task is not related to the other SW scoped label themes`

#### Priority Labels

These labels are [`prioritized`](https://docs.gitlab.com/ee/user/project/labels.html#label-priority), with the order being the one below.
1.  ~"Priority::Urgent" | `Task that must be dealed with prior to anything else`
2.  ~"Priority::High" | `High priority task`
3.  ~"Priority::Medium" | `Medium priority task`
4.  ~"Priority::Low" | `Low priority task` 

#### Pending Labels

These labels are [`prioritized`](https://docs.gitlab.com/ee/user/project/labels.html#label-priority), with the order being the one below. Only use these in combination with the `Status::On Hold` label mentioned below.
* ~"Pending::Cross-Subsystem" | `Task currently on hold, waiting on work or feedback from another subsystem`
* ~"Pending::Review" | `Task currently on hold, waiting on someone to review and approve`
* ~"Pending::Answer" | `Task currently on hold, waiting on a third-party answer, usually in the form of an email `
* ~"Pending::Meeting" | `Task currently on hold, waiting on a meeting to be carried out, since we can't go through without`
* ~"Pending::Discussion" | `Task currently on hold, waiting on discussion among the team members (and not only)`

#### Status Labels

Each task created should have a `Status::xyz` label.

* ~"Status::Up For Grabs" | `Tasks that have no assignees (yet) and are available for anyone in the subsystem to uptake`
* ~"Status::Help Needed" | `Tasks where the assignee is looking for volunteers to aid in their completion`
* ~"Status::Scheduled" | `Task currently on hold, work scheduled to resume`
* ~"Status::In Progress" | `Task currently being worked on`
* ~"Status::On Hold" | `Task currently on hold, something might be pending`
* ~"Status::Abandoned" | `Tasks that were not completed`
